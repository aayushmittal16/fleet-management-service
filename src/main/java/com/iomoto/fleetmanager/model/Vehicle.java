package com.iomoto.fleetmanager.model;

import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@XmlRootElement
@Document
public class Vehicle {
  @NotNull String name;
  @Id @NotNull String vin;
  @NotNull String licensePlateNumber;
  Map<String, String> properties = new HashMap<>();
}
