package com.iomoto.fleetmanager.repository;

import com.iomoto.fleetmanager.model.Vehicle;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehiclesRepository extends CrudRepository<Vehicle, String> {
  List<Vehicle> findAll();
}
