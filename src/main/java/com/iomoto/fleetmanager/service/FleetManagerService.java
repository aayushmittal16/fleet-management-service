package com.iomoto.fleetmanager.service;

import com.iomoto.fleetmanager.exception.NotFoundException;
import com.iomoto.fleetmanager.exception.VehicleAlreadyExistsException;
import com.iomoto.fleetmanager.model.Vehicle;
import com.iomoto.fleetmanager.repository.VehiclesRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FleetManagerService {
  private final VehiclesRepository vehiclesRepository;

  public List<Vehicle> getVehicles() {
    return vehiclesRepository.findAll();
  }

  public void createVehicle(Vehicle vehicle) throws VehicleAlreadyExistsException {
    if (vehiclesRepository.existsById(vehicle.getVin())) {
      throw new VehicleAlreadyExistsException();
    }

    vehiclesRepository.save(vehicle);
  }

  public void updateVehicle(String vin, Vehicle updated) {
    Vehicle existing = vehiclesRepository.findById(vin).orElseThrow(NotFoundException::new);
    existing.setName(updated.getName());
    existing.setLicensePlateNumber(updated.getLicensePlateNumber());
    existing.setProperties(updated.getProperties());
    vehiclesRepository.save(existing);
  }

  public void deleteVehicle(String vin) {
    vehiclesRepository.deleteById(vin);
  }

  public Vehicle getVehicle(String vehicleId) {
    return vehiclesRepository.findById(vehicleId).orElseThrow(NotFoundException::new);
  }
}
