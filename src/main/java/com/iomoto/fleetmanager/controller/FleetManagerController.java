package com.iomoto.fleetmanager.controller;

import com.iomoto.fleetmanager.exception.VehicleAlreadyExistsException;
import com.iomoto.fleetmanager.model.Vehicle;
import com.iomoto.fleetmanager.service.FleetManagerService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(
    value = "/api/v1/fleet-manager",
    produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class FleetManagerController {
  private final FleetManagerService fleetManagerService;
  public static final String VEHICLES_BASE_URL = "/vehicles";
  public static final String VEHICLES_BY_VEHICLE_ID_URL = "/vehicles/{vehicleId}";

  @GetMapping(VEHICLES_BASE_URL)
  public List<Vehicle> getVehicles() {
    return fleetManagerService.getVehicles();
  }

  @GetMapping(VEHICLES_BY_VEHICLE_ID_URL)
  public Vehicle getVehicle(@PathVariable String vehicleId) {
    return fleetManagerService.getVehicle(vehicleId);
  }

  @PostMapping(
      value = VEHICLES_BASE_URL,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  public void createVehicle(@RequestBody Vehicle vehicle) throws VehicleAlreadyExistsException {
    fleetManagerService.createVehicle(vehicle);
  }

  @PutMapping(
      value = VEHICLES_BY_VEHICLE_ID_URL,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  public void updateVehicle(@PathVariable String vehicleId, @RequestBody Vehicle vehicle) {
    fleetManagerService.updateVehicle(vehicleId, vehicle);
  }

  @DeleteMapping(VEHICLES_BY_VEHICLE_ID_URL)
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteVehicle(@PathVariable String vehicleId) {
    fleetManagerService.deleteVehicle(vehicleId);
  }
}
