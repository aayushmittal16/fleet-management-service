package com.iomoto.fleetmanager.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.iomoto.fleetmanager.exception.NotFoundException;
import com.iomoto.fleetmanager.exception.VehicleAlreadyExistsException;
import com.iomoto.fleetmanager.fixture.VehicleFixture;
import com.iomoto.fleetmanager.model.Vehicle;
import com.iomoto.fleetmanager.repository.VehiclesRepository;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class FleetManagerServiceTest {
  @MockBean private VehiclesRepository vehiclesRepository;

  @Autowired private FleetManagerService fleetManagerService;

  @Test
  @DisplayName("call to getVehicles returns vehicles from the repository")
  public void testGetVehicles() {
    List<Vehicle> expected = List.of(VehicleFixture.VEHICLE_1);
    when(vehiclesRepository.findAll()).thenReturn(expected);
    List<Vehicle> vehicles = fleetManagerService.getVehicles();
    assertEquals(expected, vehicles);
  }

  @Test
  @DisplayName("call to createVehicle saves vehicle to the repository")
  public void testCreateVehicle() throws VehicleAlreadyExistsException {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    fleetManagerService.createVehicle(vehicle);
    verify(vehiclesRepository).save(eq(vehicle));
  }

  @Test
  @DisplayName("call to updateVehicle updates vehicle in the repository")
  public void testUpdateVehicle() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    when(vehiclesRepository.findById(vehicle.getVin())).thenReturn(Optional.of(vehicle));
    Vehicle updated = VehicleFixture.clone(vehicle);
    updated.getProperties().put("color", "red");
    fleetManagerService.updateVehicle(vehicle.getVin(), updated);
    verify(vehiclesRepository).save(eq(updated));
  }

  @Test
  @DisplayName("call to updateVehicle throws exception when vehicle not found")
  public void testUpdateVehicleThrowsExceptionWhenVehicleNotFound() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    when(vehiclesRepository.findById(vehicle.getVin())).thenReturn(Optional.empty());
    assertThrows(
        NotFoundException.class,
        () -> fleetManagerService.updateVehicle(vehicle.getVin(), vehicle));
  }

  @Test
  @DisplayName("call to deleteVehicle deletes vehicle from the repository")
  public void testDeleteVehicle() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    fleetManagerService.deleteVehicle(vehicle.getVin());
    verify(vehiclesRepository).deleteById(eq(vehicle.getVin()));
  }

  @Test
  @DisplayName("call to getVehicle returns vehicle from the repository")
  public void testGetVehicle() {
    Vehicle expected = VehicleFixture.VEHICLE_1;
    when(vehiclesRepository.findById(expected.getVin())).thenReturn(Optional.of(expected));
    Vehicle vehicles = fleetManagerService.getVehicle(expected.getVin());
    assertEquals(expected, vehicles);
  }

  @Test
  @DisplayName("call to getVehicle throws exception when vehicle not found")
  public void testGetVehicleWhenVehicleNotFound() {
    String vehicleId = "VIN321";
    when(vehiclesRepository.findById(vehicleId)).thenReturn(Optional.empty());
    assertThrows(NotFoundException.class, () -> fleetManagerService.getVehicle(vehicleId));
  }
}
