package com.iomoto.fleetmanager.fixture;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iomoto.fleetmanager.model.Vehicle;
import java.util.Map;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

public class VehicleFixture {
  private static final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();

  public static final Vehicle VEHICLE_1 =
      Vehicle.builder()
          .name("Toyota Camry")
          .vin("VIN123")
          .licensePlateNumber("BER 102")
          .properties(Map.of("color", "yellow", "year", "1995"))
          .build();

  public static Vehicle clone(Vehicle obj) {
    try {
      String json = objectMapper.writeValueAsString(obj);
      return objectMapper.readValue(json, Vehicle.class);
    } catch (JsonProcessingException e) {
      return null;
    }
  }
}
