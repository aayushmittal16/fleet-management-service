package com.iomoto.fleetmanager.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.iomoto.fleetmanager.BaseIntegrationTest;
import com.iomoto.fleetmanager.fixture.VehicleFixture;
import com.iomoto.fleetmanager.model.Vehicle;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

@DataMongoTest(excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class)
public class VehicleRepositoryTest extends BaseIntegrationTest {
  @Autowired private VehiclesRepository vehiclesRepository;

  @AfterEach
  void cleanUp() {
    this.vehiclesRepository.deleteAll();
  }

  @Test
  public void testFindAll() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    vehiclesRepository.save(vehicle);

    assertEquals(List.of(vehicle), vehiclesRepository.findAll());
  }
}
