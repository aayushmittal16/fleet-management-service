package com.iomoto.fleetmanager.controller;

import static com.iomoto.fleetmanager.controller.FleetManagerController.VEHICLES_BASE_URL;
import static com.iomoto.fleetmanager.controller.FleetManagerController.VEHICLES_BY_VEHICLE_ID_URL;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iomoto.fleetmanager.exception.NotFoundException;
import com.iomoto.fleetmanager.fixture.VehicleFixture;
import com.iomoto.fleetmanager.model.Vehicle;
import com.iomoto.fleetmanager.service.FleetManagerService;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(FleetManagerController.class)
public class FleetManagerControllerTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private FleetManagerService fleetManagerService;

  private final ObjectMapper jsonMapper = Jackson2ObjectMapperBuilder.json().build();
  private final ObjectMapper xmlMapper = Jackson2ObjectMapperBuilder.xml().build();
  private static final String BASE_URL = "/api/v1/fleet-manager";

  @Test
  @DisplayName("Call to GET /api/v1/fleet-manager/vehicles returns all vehicles")
  public void testGetVehicles() throws Exception {
    List<Vehicle> vehicles = List.of(VehicleFixture.VEHICLE_1);
    when(fleetManagerService.getVehicles()).thenReturn(vehicles);
    mockMvc
        .perform(get(BASE_URL + VEHICLES_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(content().json(jsonMapper.writeValueAsString(vehicles)));
  }

  @Test
  @Disabled // Seems like this is still a problem:
  // https://github.com/FasterXML/jackson-databind/issues/2900
  @DisplayName("Call to GET /api/v1/fleet-manager/vehicles returns all vehicles in XML format")
  public void testGetVehiclesInXMLFormat() throws Exception {
    List<Vehicle> vehicles = List.of(VehicleFixture.VEHICLE_1);
    when(fleetManagerService.getVehicles()).thenReturn(vehicles);
    mockMvc
        .perform(get(BASE_URL + VEHICLES_BASE_URL).accept(MediaType.APPLICATION_XML))
        .andExpect(status().isOk())
        .andExpect(content().xml(xmlMapper.writeValueAsString(vehicles)));
  }

  @Test
  @DisplayName("Call to POST /api/v1/fleet-manager/vehicles creates a vehicle")
  public void testCreateVehicle() throws Exception {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    doNothing().when(fleetManagerService).createVehicle(vehicle);
    mockMvc
        .perform(
            post(BASE_URL + VEHICLES_BASE_URL)
                .content(jsonMapper.writeValueAsString(vehicle))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
    verify(fleetManagerService).createVehicle(vehicle);
  }

  @Test
  @DisplayName("Call to POST /api/v1/fleet-manager/vehicles using XML creates a vehicle")
  public void testCreateVehicleWithXMLInput() throws Exception {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    doNothing().when(fleetManagerService).createVehicle(vehicle);
    mockMvc
        .perform(
            post(BASE_URL + VEHICLES_BASE_URL)
                .content(xmlMapper.writeValueAsString(vehicle))
                .contentType(MediaType.APPLICATION_XML))
        .andExpect(status().isOk());
    verify(fleetManagerService).createVehicle(vehicle);
  }

  @Test
  @DisplayName("Call to PUT /api/v1/fleet-manager/vehicles/{vehicleId} updates a vehicle")
  public void testUpdateVehicle() throws Exception {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    doNothing().when(fleetManagerService).updateVehicle(vehicle.getVin(), vehicle);
    mockMvc
        .perform(
            put(BASE_URL + VEHICLES_BY_VEHICLE_ID_URL, "VIN123")
                .content(jsonMapper.writeValueAsString(vehicle))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
    verify(fleetManagerService).updateVehicle(vehicle.getVin(), vehicle);
  }

  @Test
  @DisplayName(
      "Call to PUT /api/v1/fleet-manager/vehicles/{vehicleId} with XML input updates a vehicle")
  public void testUpdateVehicleWithXMLInput() throws Exception {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    doNothing().when(fleetManagerService).updateVehicle(vehicle.getVin(), vehicle);
    mockMvc
        .perform(
            put(BASE_URL + VEHICLES_BY_VEHICLE_ID_URL, "VIN123")
                .content(xmlMapper.writeValueAsString(vehicle))
                .contentType(MediaType.APPLICATION_XML))
        .andExpect(status().isOk());
    verify(fleetManagerService).updateVehicle(vehicle.getVin(), vehicle);
  }

  @Test
  @DisplayName("Call to DELETE /api/v1/fleet-manager/vehicles/{vehicleId} deletes a vehicle")
  public void testDeleteVehicle() throws Exception {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    doNothing().when(fleetManagerService).deleteVehicle(vehicle.getVin());
    mockMvc
        .perform(
            delete(BASE_URL + VEHICLES_BY_VEHICLE_ID_URL, "VIN123")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isAccepted());
    verify(fleetManagerService).deleteVehicle(vehicle.getVin());
  }

  @Test
  @DisplayName("Call to GET /api/v1/fleet-manager/vehicles/{vehicleId} returns the vehicle")
  public void testGetVehicle() throws Exception {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    when(fleetManagerService.getVehicle(vehicle.getVin())).thenReturn(vehicle);
    mockMvc
        .perform(get(BASE_URL + VEHICLES_BY_VEHICLE_ID_URL, "VIN123"))
        .andExpect(status().isOk())
        .andExpect(content().json(jsonMapper.writeValueAsString(vehicle)));
  }

  @Test
  @DisplayName("Call to GET /api/v1/fleet-manager/vehicles/{vehicleId} returns 404")
  public void testGetVehicleThrowsException() throws Exception {
    when(fleetManagerService.getVehicle("asdas")).thenThrow(new NotFoundException());
    mockMvc
        .perform(get(BASE_URL + VEHICLES_BY_VEHICLE_ID_URL, "asdas"))
        .andExpect(status().isNotFound());
  }
}
