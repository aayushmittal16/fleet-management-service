package com.iomoto.fleetmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.iomoto.fleetmanager.fixture.VehicleFixture;
import com.iomoto.fleetmanager.model.Vehicle;
import com.iomoto.fleetmanager.repository.VehiclesRepository;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient(registerRestTemplate = true)
public class FleetManagerIntegrationTest extends BaseIntegrationTest {

  // TODO: Remove deprecated code
  @LocalServerPort private int port;

  @Autowired private RestTemplate restTemplate;

  @Autowired private VehiclesRepository vehiclesRepository;

  @AfterEach
  void cleanUp() {
    this.vehiclesRepository.deleteAll();
  }

  @Test
  public void testGetVehicle() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    vehiclesRepository.save(vehicle);
    ResponseEntity<Vehicle> response =
        restTemplate.getForEntity(
            urlWithBasePath("/vehicles/{vehicleId}"), Vehicle.class, vehicle.getVin());
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(vehicle, response.getBody());
  }

  @Test
  public void testGetVehicles() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    vehiclesRepository.save(vehicle);
    ResponseEntity<List<Vehicle>> response =
        restTemplate.exchange(
            urlWithBasePath("/vehicles"),
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<
                List<Vehicle>>() {}); // Compilation failure without explicity type mention here
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(List.of(vehicle), response.getBody());
  }

  @Test
  public void testCreateVehicle() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    ResponseEntity<Void> response =
        restTemplate.postForEntity(urlWithBasePath("/vehicles/"), vehicle, Void.class);
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(vehicle, vehiclesRepository.findById(vehicle.getVin()).orElse(null));
  }

  @Test
  public void testUpdateVehicle() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    vehiclesRepository.save(vehicle);
    vehicle.setName("Mercedes Benz");

    String url = urlWithBasePath("/vehicles/{vehicleId}");
    ResponseEntity<Void> response =
        restTemplate.exchange(
            url, HttpMethod.PUT, new HttpEntity<>(vehicle), Void.class, vehicle.getVin());
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(vehicle, vehiclesRepository.findById(vehicle.getVin()).orElse(null));
  }

  @Test
  public void testDeleteVehicle() {
    Vehicle vehicle = VehicleFixture.VEHICLE_1;
    vehiclesRepository.save(vehicle);

    String url = urlWithBasePath("/vehicles/{vehicleId}");
    ResponseEntity<Void> response =
        restTemplate.exchange(url, HttpMethod.DELETE, null, Void.class, vehicle.getVin());
    assertEquals(202, response.getStatusCodeValue());
    assertTrue(vehiclesRepository.findById(vehicle.getVin()).isEmpty());
  }

  private String urlWithBasePath(String uri) {
    return "http://localhost:" + port + "/api/v1/fleet-manager" + uri;
  }
}
