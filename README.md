# Fleet Management Service
This service exposes API endpoints to manage vehicles as part of iomoto fleet. API endpoints allow you to create, update, delete, and retrieve vehicles that are part of the fleet.

## Tech Stack
* Java 11
* Spring Boot 2.7
* MongoDB
* Docker

## API Endpoints

| Endpoint                                          | Description                                                                                | Status Codes |
|---------------------------------------------------|--------------------------------------------------------------------------------------------|--------------|
| GET /api/v1/fleet-manager/vehicles                | Returns all vehicles                                                                       | 200          |
| GET /api/v1/fleet-manager/vehicles/{vehicleId}    | Returns the vehicle by vehicle ID                                                          | 200          |
| POST /api/v1/fleet-manager/vehicles               | Creates a new vehicle                                                                      | 200, 409     |
| PUT /api/v1/fleet-manager/vehicles/{vehicleId}    | Updates the vehicle by vehicle ID. VIN cannot be updated since it's the unique identifier. | 200, 404     |
| DELETE /api/v1/fleet-manager/vehicles/{vehicleId} | Deletes the vehicle by vehicle ID                                                          | 202          |

JSON Payload:
```
{
  "name": "string",
  "vin": "string",
  "licensePlateNumber": "string",
  "properties": {
    "additionalProp1": "string",
    "additionalProp2": "string",
    "additionalProp3": "string"
    ...
    ...
  }
}
```

XML Payload:
```
<?xml version="1.0" encoding="UTF-8"?>
<Vehicle>
	<name>string</name>
	<vin>string</vin>
	<licensePlateNumber>string</licensePlateNumber>
	<properties>
		<additionalProp1>string</additionalProp1>
		<additionalProp2>string</additionalProp2>
		<additionalProp3>string</additionalProp3>
		...
		...
	</properties>
</Vehicle>
```

[Swagger UI](http://localhost:8080/swagger-ui/index.html)

## How to run this service

One line command:
```
docker-compose up
```

To execute tests:
```
mvnw test
```

## Notes
* One test does not work because of a bug in Jackson library related to Mock response matcher for new Java 9+ collection types such as `List.of` so I disabled it for the sake of time.
* Retrieving vehicles API can get really complicated to support searching by various fields so I did not get into it.